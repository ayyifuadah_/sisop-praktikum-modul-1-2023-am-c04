#!/bin/bash

# Get current hour in 24-hour format
hour=$(date "+%H")

# Determine number of times to download file based on current hour
countx=$(($hour == "00" ? 1 : $hour))

# Create folder to store downloaded files
folder_count=$(find . -maxdepth 1 -type d -name "kumpulan_*" | wc -l)

if [ "$folder_count" -eq 0 ]; then
  nama_folder="kumpulan_1.FOLDER"
else
  next_number=$((folder_count + 1))
  nama_folder="kumpulan_${next_number}.FOLDER"
fi

mkdir -p "$nama_folder"

echo "File akan didownload sebanyak $countx kali"

# Loop to download file specified number of times
for ((index=1; index<=$countx; index++)); do
  # Set filename to download
  nama_file="perjalanan_${index}.FILE"

  # Download file
  wget -O "${nama_folder}/${nama_file}" https://www.nationsonline.org/gallery/Indonesia/Piaynemo-West-Papua.jpg

  # Move file to folder
  mv "${nama_folder}/${nama_file}" "$nama_folder"

  echo "File berhasil didownload ke folder $nama_folder"
  
  # Wait 10 hours before downloading next file
  sleep 36000
done

# Define variables for zip file name and folder count
zip_name="devil_$(ls -d devil_* 2>/dev/null | wc -l | awk '{print $1+1}')"
folder_count=$(ls -d kumpulan_* 2>/dev/null | wc -l)

# Zip folders based on modulo of folder count
if [ $(expr $folder_count % 5) -eq 3 ]
then
    zip -r $zip_name kumpulan_$((folder_count-2)) kumpulan_$((folder_count-1)) kumpulan_$folder_count
elif [ $(expr $folder_count % 5) -eq 0 ]
then
    zip -r $zip_name kumpulan_$((folder_count-1)) kumpulan_$folder_count
fi

# Define variable for number of files to download based on current hour
current_hour=$(date +"%H")
if [ $current_hour -eq 0 ]; then
    num_files=1
else
    num_files=$current_hour
fi

# Create new folder for downloaded files
new_folder_name="kumpulan_$((folder_count+1))"
mkdir "$new_folder_name"

# Download files and save them to the new folder
for ((i=1; i<=num_files ;i++)) 
do
    filename="perjalanan_$i.FILE"
    wget -O "$new_folder_name/$filename" https://source.unsplash.com/1600x900/?indonesia
done

echo "Files downloaded and saved to $new_folder_name"
