#!/bin/bash

#mengambil top 5 di jepang
 echo 'berikut adalah top 5 universitas di jepang'
 cat '2023 QS World University Rankings.csv' | grep -i "Japan" | sort -t "," -k1 -n| head -n 5| awk -F "," '{print $1, $2, $4}'
 echo -e '\n'
 
#mengambil yang field fsr score paling rendah
 echo "berikut adalah univ jepang dengan fsr score terendah"
 cat '2023 QS World University Rankings.csv' | grep -i "Japan" | sort -t "," -k1 -rn| head -n 5| sort -t "," -k9 -n| awk -F "," '{print $1, $2, $9}'  
 echo -e '\n' 

#mengambil univ dengan field ger rank paling tinggi
 echo "berikut adalah univ dengan ger rank tertinggi"
 cat '2023 QS World University Rankings.csv' |grep -i "japan" | sort -t "," -k20 -n| head -n 10| awk -F "," '{print $1, $2, $20}' 
 echo -e '\n'
 
#mengambil univ dengan kata kunci keren
 echo "berikut univ dengan kata kunci keren"
 cat '2023 QS World University Rankings.csv' | grep -i "Keren" | awk -F "," '{print $1, $2}'
