#! /bin/bash

#mengakses filelog yang paling mutakhir di folder ini

fileLog="$(ls | awk '/.txt/' | tail -n 1)"

log_asli="$fileLog asli.txt"
jam=$(ls | awk '/.txt/' | tail -n 1 | cut -d':' -f1)
#echo $jam

case $jam in
	00)
	echo "$(cat ""$fileLog"" >> "$log_asli")"
	;;
	01)
	echo "$(cat ""$fileLog"" | tr [b-za-a] [a-z] | tr [B-ZA-A] [A-Z] >> "$log_asli")"
	;;
	02)
	echo "$(cat ""$fileLog"" | tr [c-za-b] [a-z] | tr [C-ZA-B] [A-Z] >> "$log_asli")"
	;;
	03)
	echo "$(cat ""$fileLog"" | tr [d-za-c] [a-z] | tr [D-ZA-C] [A-Z] >> "$log_asli")"
	;;
	04)
	echo "$(cat ""$fileLog"" | tr [e-za-d] [a-z] | tr [E-ZA-D] [A-Z] >> "$log_asli")"
	;;
	05)
	echo "$(cat ""$fileLog"" | tr [f-za-e] [a-z] | tr [F-ZA-E] [A-Z] >> "$log_asli")"
	;;
	06)
	echo "$(cat ""$fileLog"" | tr [g-za-f] [a-z] | tr [G-ZA-F] [A-Z] >> "$log_asli")"
	;;
	07)
	echo "$(cat "$fileLog" | tr [h-za-g] [a-z] | tr [H-ZA-G] [A-Z] >> "$log_asli")"
	;;
	08)
	echo "$(cat "$fileLog" | tr [i-za-h] [a-z] | tr [I-ZA-H] [A-Z] >> "$log_asli")"
	;;
	09)
	echo "$(cat "$fileLog" | tr [j-za-i] [a-z] | tr [J-ZA-I] [A-Z] >> "$log_asli")"
	;;
	10)
	echo "$(cat "$fileLog" | tr [k-za-j] [a-z] | tr [K-ZA-J] [A-Z] >> "$log_asli")"
	;;
	11)
	echo "$(cat "$fileLog" | tr [l-za-k] [a-z] | tr [L-ZA-K] [A-Z] >> "$log_asli")"
	;;
	12)
	echo "$(cat "$fileLog" | tr [m-za-l] [a-z] | tr [M-ZA-L] [A-Z] >> "$log_asli")"
	;;
	13)
	echo "$(cat "$fileLog" | tr [n-za-m] [a-z] | tr [N-ZA-M] [A-Z] >> "$log_asli")"
	;;
	14)
	echo "$(cat "$fileLog" | tr [o-za-n] [a-z] | tr [O-ZA-N] [A-Z] >> "$log_asli")"
	;;
	15)
	echo "$(cat "$fileLog" | tr [p-za-o] [a-z] | tr [P-ZA-O] [A-Z] >> "$log_asli")"
	;;
	16)
	echo "$(cat "$fileLog" | tr [q-za-p] [a-z] | tr [Q-ZA-P] [A-Z] >> "$log_asli")"
	;;
	17)
	echo "$(cat "$fileLog" | tr [r-za-q] [a-z] | tr [R-ZA-Q] [A-Z] >> "$log_asli")"
	;;
	18)
	echo "$(cat "$fileLog" | tr [s-za-r] [a-z] | tr [S-ZA-R] [A-Z] >> "$log_asli")"
	;;
	19)
	echo "$(cat "$fileLog" | tr [t-za-s] [a-z] | tr [T-ZA-S] [A-Z] >> "$log_asli")"
	;;
	20)
	echo "$(cat "$fileLog" | tr [u-za-t] [a-z] | tr [U-ZA-T] [A-Z] >> "$log_asli")"
	;;
	21)
	echo "$(cat "$fileLog" | tr [v-za-u] [a-z] | tr [V-ZA-U] [A-Z] >> "$log_asli")"
	;;
	22)
	echo "$(cat "$fileLog" | tr [w-za-v] [a-z] | tr [W-ZA-V] [A-Z] >> "$log_asli")"
	;;
	23)
	echo "$(cat "$fileLog" | tr [x-za-w] [a-z] | tr [X-ZA-W] [A-Z] >> "$log_asli")"
	;;
esac
